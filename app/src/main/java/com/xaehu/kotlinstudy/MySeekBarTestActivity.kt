package com.xaehu.kotlinstudy

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.PersistableBundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_seekbar_test.*

class MySeekBarTestActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(R.layout.activity_seekbar_test)
        btn.setOnClickListener{
            val text = et.text.toString()
            if(!TextUtils.isEmpty(text)){

            }
        }

        mySeekBar.setRadius(20f)
        mySeekBar.invalidate()
        tv.setText(""+mySeekBar.currentProgress)
        mySeekBar.setOnDragListener(MySeekBar.OnDragListener {
            tv.setText(""+it)
        })
    }
}
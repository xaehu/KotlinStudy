package com.xaehu.kotlinstudy.roomstudy

import android.content.Context
import androidx.room.Room

open class MyDateBaseHelper(context: Context) {
    private val build:MyDb = Room
        .databaseBuilder(context, MyDb::class.java, "my_db_test")
        .fallbackToDestructiveMigration()
        .build()

    companion object {
        @Volatile
        private var instance: MyDateBaseHelper? = null
        fun getInstance(context: Context): MyDateBaseHelper? {
            if (instance == null) {
                synchronized(MyDateBaseHelper::class.java) {
                    if (instance == null) {
                        instance = MyDateBaseHelper(context)
                    }
                }
            }
            return instance
        }
    }

    fun getStudentDao():StudentDao{
        return build.getStudentDao()
    }

}
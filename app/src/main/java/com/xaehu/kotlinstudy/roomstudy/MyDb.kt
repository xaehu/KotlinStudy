package com.xaehu.kotlinstudy.roomstudy

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Student::class],version = 2)
abstract class MyDb constructor():RoomDatabase(){

    abstract fun getStudentDao():StudentDao
}
package com.xaehu.kotlinstudy

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*


class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivityLog"
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener {
            GlobalScope.launch(Dispatchers.Main) {
                Log.i(TAG, "GlobalScope.launch")
                textView.text = "稍等……"
                delay(1000)
                startActivity(Intent(this@MainActivity,RoomTestActivity::class.java))
                textView.text = "点击按钮打开界面"
            }
        }

        textView.setOnClickListener {
            Toast.makeText(this,"click textView",Toast.LENGTH_SHORT).show()
        }
    }

    private suspend fun sleep500ms(id:Int):String{
        Log.i(TAG, "sleep500ms: ")
        var re = ""
        withContext(Dispatchers.IO){
            Thread.sleep(1000)
            re = "sult"
        }
        Log.i(TAG, "sleep500ms: sleep end")
        return re
    }

}